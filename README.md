```
FROM node:14 # a partir de qual imagem será criada
WORKDIR /app-node # diretório de trabalho
ARG PORT_BUILD=3000 # define variável na criação da imagem
ENV PORT=$PORT_BUILD
EXPOSE $PORT # documenta que o container vai ser executado na porta 3000
COPY . /app-node # copia tudo para a pasta app-node do container
RUN npm install #executa na criação da imagem
ENTRYPOINT npm start # executa após o container subir

# salvar o arquivo e executar: docker build -t test/app:1.0 . 
```


********************* DOCKER *********************

#### CRIANDO UM CONTAINER E ACESSANDO O MODO ITERATIVO
```
docker container run -ti centos:7
```

#### PARA SAIR DO CONTAINER E DEIXAR ELE RODANDO
```
segurar ctrl + p + q 
```

#### PARA VOLTAR AO CONTAINER EM EXECUÇÃO NO MODO ITERATIVO
```
docker container attach <ID_CONTAINER>
```

#### CRIAR UM CONTAINER SEM INICIALIZA-LO
```
docker container create -ti ubuntu 
```


#### PARA EXECUTAR UM CONTAINER RECEM CRIADO, COMO ACIMA, BASTA USAR OS COMANDOS
```
docker container start  <ID_CONTAINER>
docker container attach <ID_CONTAINER>
```


#### PARAR UM CONTAINER EM EXECUÇÃO
```
docker container stop <ID_CONTAINER>
```

#### É POSSÍVEL REINICIAR UM CONTAINER
```
docker container restart <ID_CONTAINER>
```


#### É POSSÍVEL PAUSAR UM CONTAINER
```
docker container pause <ID_CONTAINER>
```


#### PARA "DESPAUSAR" UM CONTAINER
```
docker container unpause <ID_CONTAINER>
```


#### VISUALIZANDO CONSUMO DE RECURSOS DE UM CONTAINER
```
docker container stats <ID_CONTAINER>
```


#### VISUALIZANDO CONSUMO DE RECURSOS DE TODOS CONTAINERS
```
docker container stats 
```

#### ACESSANDO TERMINAL DE UM CONTAINER
```
docker exec -it <ID_CONTAINER> /bin/bash
```


#### VISUALIZANDO PROCESSOS DE UM CONTAINER
```
docker container top <ID_CONTAINER>
```


#### VERIFICANDO LOG DE UM CONTAINER
```
docker container logs <ID_CONTAINER>
```

#### VERIFICANDO LOG DE UM CONTAINER DE FORMA DINÂMICA
```
docker container logs -f <ID_CONTAINER> 
```

#### REMOVENDO UM CONTAINER
```
docker container rm <ID_CONTAINER>
```

#### REMOVENDO UM CONTAINER A FORÇA
```
docker container rm -f <ID_CONTAINER>
```


#### DAR NOME A UM CONTAINER
```
docker container run -ti --name <NOME_CONTAINER> debian
```


#### INSPECIONANDO UM CONTAINER
```
docker container inspect <NOME_CONTAINER>
docker container inspect <NOME_CONTAINER> | grep -i mem
docker container inspect <NOME_CONTAINER> | grep -i cpu
```


#### SUBINDO UM NOVO CONTAINER UTILIZANDO NO MÁXIMO 512 MB DE MEMORIA
```
docker container run -ti -m 512m --name <NOME_CONTAINER> debian
```

#### SUBINDO UM NOVO CONTAINER UTILIZANDO NO MÁXIMO 1/2 CPU
```
docker container run --cpus=0.5 --name teste1 nginx
```

#### ALTERANDO MEMÓRIA E CPU 
```
docker container update -m 256m --cpus=1 <NOME_CONTAINER>
```

#### VISUALIZAR LOGS
```
docker logs -f <NOME_CONTAINER>
```


********************* DOCKERFILE *********************
```
ADD           Copia novos arquivos, diretórios, arquivos TAR ou arquivos remotos e adiciona ao filesystem do container.
CMD           Executa um comando. Diferentemente do RUN, que executa o comando em que está "buildando" a imagem, o CMD irá faze-lo quando o container for iniciado.
LABEL         Adiciona metadados à imagem, como versão, descrição e fabricante.
COPY          Copia novos arquivos e diretórios e os adiciona ao filesystem do container
ENTRYPOINT    Permite que você configure um container para rodar um executável. Quando esse executável for iniciado, o container também será.
ENV           Informa variáveis de ambiente ao container
EXPOSE        Informa qual porta do container estará ouvindo
FROM          Indica qual imagem será utilizada como base. Ela precisa ser a primeira linha do Dockerfile
MAINTAINER    Autor da imagem
RUN           Executa qualquer comando em uma nova camada no topo da imagem e "comita" as alterações. Essas alterações você poderá utilizar nas próximas instruções de seu Dcokerfile.
USER          Determina qual usuário será utilizado na imagem. Por padrão é o "root"
VOLUME        Permite a criação de um ponto de montagem no container
WORKDIR       Responsável por mudar o diretório "/" raiz para o especificado nele

flag (file) -f = indica o nome do arquivo dockerfile
flag (tag)  -t = indica o nome do usuário/autor da imagem
ATENÇÃO AO PONTO NO FINAL(.)
```
---

```
FROM debian
RUN /bin/echo "HELLO DOCKER"
```
```
docker build -f primeiro_dockerfile.dockerfile -t gardim/hello:1.0 .
```


```
FROM golang
WORKDIR /app
ADD . /app
RUN go build -o goapp
ENTRYPOINT ./goapp
```
```
docker build -t gardim/goapp:1.0 . 
```


********************* VOLUMES *********************

#### CRIANDO VOLUME DA MANEIRA ANTIGA
```
docker container run -ti --mount type=bind,src=/home/jo/Documentos/estudo/docker/volume,dst=/volume ubuntu
```

#### CRIANDO VOLUME DA MANEIRA ANTIGA COMO READY-ONLY
```
docker container run -ti --mount type=bind,src=/home/jo/Documentos/estudo/docker/volume,dst=/volume,ro ubuntu
```


#### NOVA MANEIRA DE CRIAR VOLUMES
```
docker volume create <NOME_VOLUME>
```


#### NOVA MANEIRA DE REMOVER VOLUMES
```
docker volume rm <NOME_VOLUME>
```

#### VERIFICAR DETALHES DO VOLUME
```
docker volume inspect <NOME_VOLUME>
```

#### REMOVER VOLUMES QUE NÃO ESTÃO SENDO UTILZADOS 
```
docker volume prune
```

#### PARA MONTAR O VOLUME CRIADO EM ALGUM CONTAINER, BASTA EXECUTAR (-d não bloqueia o terminal)
```
docker container run -d --mount type=volume,source=<NOME_VOLUME>,destination=/var/opa nginx 
```

#### COMPARTILHANDO VOLUMES 
(--volumes-from usado para montar volume disponibilizado por outro container)
(-e enviroment)
```
docker container run -v /data -ti --name dbdados centos
docker run -d -p 5432:5432 --name pgsql1 --volumes-from dbdados -e POSTGRESQL_USER=docker -e POSTGRESQL_PASS=docker -e POSTGRESQL_DB=docker kamui/postgresql
docker run -d -p 5433:5433 --name pgsql2 --volumes-from dbdados -e POSTGRESQL_USER=docker -e POSTGRESQL_PASS=docker -e POSTGRESQL_DB=docker kamui/postgresql
```

#### CUSTOMIZANDO UMA IMAGEM

1- docker container run -ti debian:8 /bin/bash
2- apt-get update && apt-get install -y apache2 && apt-get clean
3- (sair do container mas deixar em execução)
4- docker container ls
5- docker commit -m "meu container" <NOME_CONTAINER>
6- docker image ls 
7- docker tag <IMAGE_ID> gardim/apache_2:1.0
8- docker container run -ti gardim/apache_2:1.0
9- /etc/init.d/apache2 start


#### SABER COMO UMA IMAGEM FOI CRIADA
```
docker history gardim/apache:1.0
```

#### LOGAR NO DOCKER HUB OU OUTRO REGISTRY PELO BASH
```
docker login
docker login registry.seilaqual.com
```

#### ENVIANDO A IMAGEM CRIADA PARA O REGISTRY
```
docker push jogardim/apache:1.0
```

#### VISUALIZANDO SEU REPOSITORIO
```
docker search <USUARIO>
```

#### CRIANDO UM DOCKER DISTRIBUTION LOCAL
```
docker container run -d -p 5000:5000 --restart=always --name registry registry:2
# para fazer o push no repositório local, mudar a tag e comitar
docker tag <IMAGE_ID> localhost:5000/apache:1.0 
docker push localhost:5000/apache:1.0 
```


**************** DOCKER COMPOSE ********************

#### docker-compose.yml
```
version: "3"
services:
	web:
		image: nginx
		deploy: 
			replicas: 5
			resources:
				limits: 
					cpus: "0.1"
					memory: 50M
			restart_policy:
				condition: on-failure
		ports:
		- "8080:80"
		networks: 
		- webserver
networks:
	webserver:
```

version: "3"	Versão do compose
services:       Início da definição do meu serviço
web:		    Nome do serviço
image: nginx    Imagem que será utilizada
deploy:         Início da estratégia de deploy
replicas: 5     Quanitdade de replicas
resources:      Início da estratégia de utilização de recursos.
limits:         Limites
cpus:			Limite do CPU
memory:			Limite de memória
restart_policy  Políticas de restart
condition: on-failure: Somente irá "restartar" o container em caso de falha
ports: 			Quais portas serão expostas
- "8080:80"		Portas expostas
networks:		Definição das redes que serão utilizadas nesse serviço
- webserver     Nome da rede do serviço
networks:		Declarando as redes que usaremos nesse docker-compose
webserver		Nome da rede a ser criada, caso não exista.       






#### docker-compose.yml
```
version: "3"
services:
	db:
		image: mysql:5.7
		volumes:
			-db_data:/var/lib/mysql
		environment:
			MYSQL_ROOT_PASSWORD: somewordpress
			MYSQL_DATABASE: wordpress
			MYSQL_USER: wordpress
			MYSQL_PASSWORD: wordpress
	wordpress:
		depends_on:
			- db
		image: wordpress:latest
		ports: 
			- "8000:80"		
		environment:
			WORDPRESS_DB_HOST: db:3306
			WORDPRESS_DB_USER: wordpress
			WORDPRESS_DB_PASSWORD: wordpress
volumes:
	db_data:
```

##### volumes: 					Definição dos volumes utilizados pelo service
##### - db_data:/var/lib/mysql	                Volume e destino
##### environment:				Definição de variáveis de ambiente utilizado pelo service






